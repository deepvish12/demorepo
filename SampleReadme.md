# Secure Plus

![Secure Plus](https://bitbucket.org/deepvish12/demorepo/raw/a504c925c81587448f57753b0d45235a756d4d3a/assets/ContactLessAttendenceBanner-1.png)

## Table of content
-   [Core Concept](#core-concept)
-   [Principle](#principle)
-   [Solution Features](#solution-features)
-   [Technologies and architecture used](#technologies-and-architecture-used)
-   [What your code is designed for](#what-your-code-is-designed-for)
-   [What your code was written in](#what-your-code-was-written-in)
-   [Demo](#demo)
-   [Open source or proprietary software used](#open-source-or-proprietary-software-used)
-   [Why it's cool](#why-its-cool)
-   [Possible Enhancements](#possible-enhancements)

## Core Concept
This is a innovative solution based on Face Recognition & Internet of things (IoT) Technology which will improve COVID-19 compliances & safety of Offices, Home , Industries and all such building premises. It also provides real time analytics & on-fly dynamic control, which is very helpful for Remote administration, Human resource department & Management. For this the concept uses Machine learning algorithms (like K-Means, Smart Recommendations, Convolution Neural Networks).

## Principle
1. Avoiding common physical point of contact which may results in spreading contamination. Such as entry/exit gates, handle of bus gates etc.
2. Minimize the direct interactions of persons. For example, security guard need not talk to incoming employees for any reason, or family member need not go to gate & inquire who the incoming person is.
3. To have an automated attendance system, that could identify the person coming and hence avoiding the thumb impression biometrics with AI Face Recognition System.
4. Identification of employees based on AI powerful face recognition system and approves entries after the successful verification from the database.
5. Avoid unauthorized person entry and foresee tragic circumstances.

## Solution Features
|<p align="center"><img src="https://secureservercdn.net/198.71.233.129/v72.354.myftpupload.com/wp-content/uploads/2020/05/touchless-sign-in.png?time=1621026818"></p>  |<p align="center"><img src="https://secureservercdn.net/198.71.233.129/v72.354.myftpupload.com/wp-content/uploads/2020/05/modern-attendance.png?time=1621026818"></p> |<p align="center"><img src="https://secureservercdn.net/198.71.233.129/v72.354.myftpupload.com/wp-content/uploads/2020/05/facial-recognition.png?time=1621026818"></p>	|
|--|--|--|
| **Touchless system** | **Modern Employee Attendance System** | **AI Face Recognition**	|

## Technologies and architecture used
- **PHP**  is a  general-purpose  scripting language especially suited to  web development. It was originally created by Danish-Canadian programmer Rasmus Lerdorf  in 1994. The PHP  reference implementation is now produced by The PHP Group. PHP originally stood for  _Personal Home Page_, but it now stands for the  recursive initialism _PHP: Hypertext Preprocessor_.
- **Python** is an interpreted  high-level general-purpose programming language. Python's design philosophy emphasizes code readability with its notable use of significant indentation. Its language constructs as well as its object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.
- Face Recognitions(Dlib python wrapper)
  A **facial recognition system** is a technology capable of matching a human face from a digital image or a video frame against a database of faces, typically employed to authenticate users through ID verification services, works by pinpointing and measuring facial features from a given image.
- Machine learning (k-means, convulation neural network)
  **_k_-means clustering** is a method of  vector quantization, originally from signal processing, that aims to partition  _n_ observations into _k_ clusters in which each observation belongs to the cluster with the nearest mean (cluster centers or cluster centroid, serving as a prototype of the cluster.
- Internet of Thing (IoT Device and aurdino)
  The **Internet of things** (**IoT**) describes the network of physical objects—a.k.a. "things"—that are embedded with sensors, software, and other technologies for the purpose of connecting and exchanging data with other devices and systems over the Internet.

### Architecture
![architecture](https://bitbucket.org/deepvish12/demorepo/raw/6a310f5eff4b0bc798dcec6f8ca460d3279cd498/assets/Innoation_Ideation_architecture.png)

## What your code is designed for
Our application Secure Plus is General Purpose, Affordable, High available, Ease of installation & maintenance solution which help improves individuals enhance safety & COVID-19 compliances of their homes, offices and other building premises.  
Using the help of IP Camera or Camera sensor attached to Raspberry PI image of a person is captured and then analyzed whether that person/employee is allowed the access of home/office-floor/building premises. Application then sends response to micro controller & then too servo motors for opening closing of gate as required. In addition to this Secure Plus provides real time analytics & smart recommendations based on where the application is installed (Home/Office etc.).  
The way it help improve COVID-19 compliances is, since as we know entry/exit point of any building premises are the common point of contact of each person. One infected person can have this common point infected too if touched physically, which will eventually result in spread of contamination if accidently touched by some other member/employee. As the system is autonomous, person/employee at the entry/exit gate need not touch the gate as they are controlled by sensors and open/closed with respect to rules/approvals by administrator of system.

## What your code was written in
Following is our technology stack:  
**1. Python3:**  Core code face comparing logic (Dlib, Face_recognition), Raspberry Pi Logic usage GPIO pins python code.  
**2. Machine Learning:**  K-means clustering(patter recognition), Convolution neural network (In face recognition)  
**3. IoT:**  We have used Raspberry Pi 4 board interfaced with Camera & aurdino (Atmel Atmega 328p microcontroller).  
**4. Web Server:**  PHP, HTML, CSS, JAVASCRIPT, Rest API Webservices.

## Demo
![Demo](https://github.com/Microsoft/vscode-chrome-debug/blob/master/images/demo.gif?raw=true)

## Open source or proprietary software used
Following is our technology stack:  
**1. Python3:**  Core code face comparing logic (Dlib, Face_recognition), Raspberry Pi Logic usage GPIO pins python code.  
**2. Machine Learning:**  K-means clustering(patter recognition), Convolution neural network (In face recognition)  
**3. IoT:**  We have used Raspberry Pi 4 board interfaced with Camera & aurdino (Atmel Atmega 328p microcontroller).  
**4. Web Server:**  PHP, HTML, CSS, JAVASCRIPT, Rest API Webservices.

## Why it's cool
A single system "Secure Plus" in smart managing of attendance, payroll, employees, and all of the above contact less system.

## Possible Enhancements
- Self Registration of Employees using Secure Plus Portal
- Face mask detection using AI driven system
- Allow or deny employee or person entry with temperature scanning